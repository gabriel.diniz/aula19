public class aula19 { //Tipos primitivos no Java II

    public static void main(String[] args) {

        boolean b = true;  // byte 1 - por padrão ele sempre será false, ocupa 1 byte

        char c = 'a';  // byte 2  - por padrão o char é igual '' vazio, ocupa 2 bytes, 16 bits
        char c2 = '1';

        byte Byte = 100; // 1 byte - por padrão 0
        byte bb = 100; //-128 e +127

        short camisa = 2000; // por padrão 0 a variavel  -32768 de +32767
        int i = 1; // por padrão 0 a variavel -2,147,483,648 a + 2,147,486,647

        long 1;

        float f = 4.4f; // inserir f
        double d = 2.2;

    }
}
